import React from "react";
import {Navigate} from "react-router-dom";
import {useAccountStore} from "../../store/store";


const CheckLogin: React.FC<{ children: React.ReactNode }> = (props) => {

    const isLogin = useAccountStore(account => {
        return account.isLogin();
    });

    return (
        <>
            {isLogin ? props.children
                : <Navigate to={"/login"} replace={true}/>}
        </>
    )
}

export default CheckLogin