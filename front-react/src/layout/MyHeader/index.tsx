import React, {useContext,} from 'react';
import {Dropdown, Menu, MenuProps} from 'antd';
import {getFirstChildPathByParent, useMenuItems} from "../../routers/router";
import {useTranslation} from "react-i18next";
import {useLocation, useNavigate} from "react-router-dom";
import {
    ExportOutlined, GlobalOutlined, KeyOutlined,
    TranslationOutlined,
    UserOutlined
} from "@ant-design/icons";
import {Header} from "antd/es/layout/layout";

import enUS from 'antd/locale/en_US';
import zhCN from 'antd/locale/zh_CN';
import dayjs from 'dayjs';
import 'dayjs/locale/zh-cn';
import {SettingsContext} from "../../context/settings";
import {useAccountStore} from "../../store/store";
import {useAccountDetail} from "../../service/AccountService";
import styles from "./MyHeader.module.scss";


const MyHeader: React.FC = () => {

    const {isSuccess, data} = useAccountDetail();


    const navigate = useNavigate();
    const {pathname} = useLocation();


    const topMenus = useMenuItems(true);
    let menuKey: string = '';
    for (let topMenu of topMenus) {
        if (pathname.startsWith(topMenu!.key + "/") || pathname === topMenu!.key) {
            menuKey = topMenu!.key as string;
        }
    }

    const {t, i18n} = useTranslation();


    function handlerTopItemClick(item: { key: string }) {
        const firstChildPath = getFirstChildPathByParent(item.key);
        navigate(firstChildPath ? firstChildPath : item.key);
    }

    const {setLocale} = useContext(SettingsContext);
    const handlerChangeLang = (e: { key: string }) => {
        i18n.changeLanguage(e.key)
        dayjs.locale(e.key);
        setLocale(e.key === "en" ? enUS : zhCN)
    }


    const langItems: MenuProps['items'] = [
        {
            key: 'zh',
            label: "简体中文",
            onClick: handlerChangeLang
        },
        {
            key: 'en',
            label: "English",
            onClick: handlerChangeLang
        },
    ];

    const account = useAccountStore();

    const handlerUserMenus = (item: { key: string }) => {
        if (item.key === "loggedOut") {
            account.logOut();
            navigate("/login")
        } else {
            navigate(item.key)
        }
    }

    const avatarItems: MenuProps['items'] = [
        {
            key: '/system/profile',
            label: <><UserOutlined/> 个人资料</>,
            onClick: handlerUserMenus
        },
        {
            key: '/system/password',
            label: <> <KeyOutlined/> 修改密码</>,
            onClick: handlerUserMenus
        },
        {
            type: 'divider',
        },
        {
            key: 'loggedOut',
            label: <>  <ExportOutlined/> 退出登录</>,
            onClick: handlerUserMenus
        },
    ];


    return (
        <Header className={styles.header}>
            <div className={styles.logo}>
                MyBatis-Flex-Admin
            </div>
            <div style={{display: "flex", justifyContent: "space-between", overflow: "hidden"}}>

                <Menu theme={"dark"} style={{background: "#00000000", minWidth: "500px"}} mode="horizontal"
                      selectedKeys={[menuKey]}
                      items={topMenus}
                      onClick={handlerTopItemClick}
                />

                <div className={styles.actions}>

                    <a href="https://mybatis-flex.com" target="_blank"
                       style={{color: "#fff"}}>
                        <GlobalOutlined style={{fontSize: "16px"}}/>
                    </a>

                    <Dropdown menu={{items: langItems}} placement="bottom">
                        <div>
                            <TranslationOutlined style={{fontSize: "16px"}}/>
                        </div>
                    </Dropdown>

                    <Dropdown menu={{items: avatarItems}} placement="bottomRight">
                        <div>
                            {isSuccess && data.account &&
                            <span><UserOutlined
                                style={{fontSize: "16px"}}/> {data.account.nickname || data?.account.userName}</span>
                            }
                        </div>
                    </Dropdown>

                </div>

            </div>

        </Header>
    );
};


export default MyHeader;