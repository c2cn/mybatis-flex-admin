import React from 'react';
import {Layout, theme} from 'antd';
import {Outlet} from "react-router-dom";
import MyBreadcrumb from "../MyBreadcrumb";
import MyHeader from "../MyHeader";
import LeftMenu from "../LeftMenu";

const {Content} = Layout;


const Portal: React.FC = () => {
    const {
        token: {colorBgContainer},
    } = theme.useToken();


    return (
        <Layout>
            <MyHeader />
            <Layout>
                <LeftMenu/>
                <Layout style={{padding: '0 24px 24px'}}>
                    <MyBreadcrumb/>
                    <Content style={{
                        padding: 24,
                        margin: 0,
                        minHeight: "calc(100vh - 126px)",
                        background: colorBgContainer,
                    }}
                    >
                        <Outlet/>
                    </Content>
                </Layout>
            </Layout>
        </Layout>
    );
};


export default Portal;