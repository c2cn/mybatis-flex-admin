import React, {useEffect, useState} from 'react';
import {
    Col,
    Form,
    Input, Modal,
    Row,
    theme,
} from 'antd';
import {
    useAccountInfo,
    useAccountSave,
    useAccountUpdate
} from "../../../service/AccountService";

const AccountDetail: React.FC<{
    editId?: number,
    onOk: () => void,
    onCancel: () => void,
    open: boolean
    title: string
}> = (props) => {

    const {token} = theme.useToken();
    const [form] = Form.useForm();
    const [confirmLoading, setConfirmLoading] = useState(false);


    const accountSave = useAccountSave();
    const accountUpdate = useAccountUpdate();
    const {isLoading, data} = useAccountInfo(props.editId!);

    useEffect(() => {
        if (!isLoading && data) {
            form.setFieldsValue(data.data)
        }
    }, [isLoading])


    const formStyle = {
        maxWidth: 'none',
        background: token.colorFillAlter,
        borderRadius: token.borderRadiusLG,
        padding: 24,
        marginBottom: '20px'
    };

    const formItemLayout = {labelCol: {span: 6}, wrapperCol: {span: 14}};


    const onFinish = (values: any) => {
        if (!!props.editId) {
            accountUpdate.mutate(values, {
                onSettled: (data, error, variables, context) => {
                    setConfirmLoading(false);
                    props.onOk();
                    form.resetFields();
                }
            });
        } else {
            accountSave.mutate(values, {
                onSettled: (data, error, variables, context) => {
                    setConfirmLoading(false);
                    props.onOk();
                    form.resetFields();
                }
            });
        }
    };


    const onCancel = () => {
        form.resetFields();
        props.onCancel();
    };

    const onOk = () => {
        setConfirmLoading(true)
        form.submit();
    };

    return (
        <Modal title={props.title}
               open={props.open}
               onOk={onOk}
               onCancel={onCancel}
               confirmLoading={confirmLoading}
               width={"60%"}>

            <Form
                {...formItemLayout}
                form={form} style={formStyle} onFinish={onFinish} labelAlign={"right"}>

                <Form.Item name="id">
                    <Input type={"hidden"}/>
                </Form.Item>

                <Row gutter={24}>

                    <Col span={20} offset={2}>
                        <Form.Item name="userName" label="用户名">
                            <Input placeholder="请输入用户名"/>
                        </Form.Item>
                    </Col>

                    <Col span={20} offset={2}>
                        <Form.Item name="password" label="密码">
                            <Input placeholder="请输入密码"/>
                        </Form.Item>
                    </Col>

                    <Col span={20} offset={2}>
                        <Form.Item name="nickname" label="昵称">
                            <Input placeholder="请输入昵称"/>
                        </Form.Item>
                    </Col>

                    <Col span={20} offset={2}>
                        <Form.Item name="email" label="Email">
                            <Input placeholder="请输入 Email"/>
                        </Form.Item>
                    </Col>

                    <Col span={20} offset={2}>
                        <Form.Item name="mobile" label="手机号">
                            <Input placeholder="请输入手机号"/>
                        </Form.Item>
                    </Col>

                </Row>
            </Form>
        </Modal>
    );
};

export default AccountDetail