import React from 'react';
import {
    Button,
    Col,
    Form,
    Input,
    Row,
    theme,
} from 'antd';
import {useSearchParams} from "react-router-dom";
import {removeEmpty} from "../../../lib/commons";


const SearchForm: React.FC<{ onSearch: (values: any) => void }>
    = (props) => {

    const [searchParams] = useSearchParams();

    const {token} = theme.useToken();
    const [form] = Form.useForm();

    const formStyle = {
        maxWidth: 'none',
        background: token.colorFillAlter,
        borderRadius: token.borderRadiusLG,
        padding: 24,
        marginBottom: '20px'

    } ;

    const onFinish = (values: any) => {
        props.onSearch(removeEmpty(values));
    };

    return (
        <Form form={form} name="advanced_search" style={formStyle} onFinish={onFinish}>
            <Row gutter={24}>

                <Col span={6} >
                    <Form.Item name="userName" label="用户名"  initialValue={ searchParams.get('userName') || ""}>
                        <Input placeholder="请输入用户名"/>
                    </Form.Item>
                </Col>

                <Col span={6} >
                    <Form.Item name="email" label="邮箱"  initialValue={ searchParams.get('email') || ""}>
                        <Input placeholder="请输入邮箱"/>
                    </Form.Item>
                </Col>

                <Col span={6} >
                    <Form.Item name="mobile" label="手机号"  initialValue={ searchParams.get('mobile') || ""}>
                        <Input placeholder="请输入手机号码"/>
                    </Form.Item>
                </Col>

                <Col span={ 6} style={{textAlign: 'right'} }>
                    <Button type="primary" htmlType="submit">
                        搜索
                    </Button>
                    <Button
                        style={{margin: '0 8px'}  }
                        onClick={() => {
                            form.resetFields();
                        }}
                    >
                        重置
                    </Button>
                </Col>
            </Row>
        </Form>
    );
};


export default SearchForm