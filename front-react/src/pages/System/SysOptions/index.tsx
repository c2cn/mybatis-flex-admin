import React, {useEffect, useState} from 'react';
import {Button, Divider, Form, Input, message, Radio, Select, Switch} from "antd";
import {Typography} from 'antd';
import {useOptionsQuery, useOptionsUpdate} from "../../../service/SysOptionService";

const {Title} = Typography;


const SysOption: React.FC = () => {
    const [form] = Form.useForm();

    const [submitLoading, setSubmitLading] = useState(false)
    const optionsUpdate = useOptionsUpdate()
    const {isSuccess, data} = useOptionsQuery();

    useEffect(() => {
        if (isSuccess && data) {
            form.setFieldsValue(data.data);
        }
    }, [isSuccess])


    const onFinish = (values: any) => {
        setSubmitLading(true)
        optionsUpdate.mutate(values, {
            onSettled: (data, error, variables, context) => {
                setSubmitLading(false);
                if (data && data.errorCode == 0 && !data.message) {
                    message.success("系统设置修改成功")
                }
            }
        });
    };

    const handleSubmit = () => {
        form.submit();
    }


    return (
        <>
            <Title level={4} style={{paddingBottom: '20px'}}>系统设置</Title>
            <Form
                layout="vertical"
                form={form}
                style={{maxWidth: 600}}
                onFinish={onFinish}
            >
                <Form.Item label="自动报警" name="autoAlarmEnable" valuePropName="checked">
                    <Switch checkedChildren="开启" unCheckedChildren="关闭"/>
                </Form.Item>
                <Form.Item label="SQL 执行消耗大于多少毫秒时报警" name="autoAlarmElapsedTime">
                    <Input placeholder="请输入毫秒数"/>
                </Form.Item>

                <Divider orientation="left" orientationMargin="0" style={{padding: "40px 0 10px 0"}}>邮件配置</Divider>

                <Form.Item label="邮箱接收报警信息" name="alarmEmailEnable" valuePropName="checked">
                    <Switch checkedChildren="开启" unCheckedChildren="关闭"/>
                </Form.Item>

                <Form.Item label="接收邮箱" name="alarmEmail">
                    <Input placeholder="请输入报警接收邮箱"/>
                </Form.Item>

                <Form.Item label="SMTP 地址" name="alarmEmailSMTP">
                    <Input placeholder="例如：smtp.qq.com"/>
                </Form.Item>

                <Form.Item label="用户名" name="alarmEmailUser">
                    <Input placeholder="例如：***@mybatis-flex.com"/>
                </Form.Item>

                <Form.Item label="密码" name="alarmEmailPassword">
                    <Input placeholder="请输入邮箱密码"/>
                </Form.Item>

                <Form.Item label="使用 SSL" name="alarmEmailSSLEnable" extra="QQ 邮箱等要求必须开启 SSL。" valuePropName="checked">
                    <Switch checkedChildren="使用" unCheckedChildren="不使用"/>
                </Form.Item>

                <Divider orientation="left" orientationMargin="0" style={{padding: "40px 0 10px 0"}}>短信配置</Divider>

                <Form.Item label="短信接收报警信息" name="alarmSmsEnable" valuePropName="checked">
                    <Switch checkedChildren="开启" unCheckedChildren="关闭"/>
                </Form.Item>

                <Form.Item label="接收手机号" name="alarmSmsMobile">
                    <Input placeholder="请输入报警接收手机号码"/>
                </Form.Item>

                <Form.Item label="短信服务商" name="alarmSmsProvider">
                    <Select
                        style={{width: 120}}
                        options={[
                            {value: 'aliyun', label: '阿里云'},
                            {value: 'qcloud', label: '腾讯云'},
                        ]}
                    />
                </Form.Item>

                <Form.Item label="AppId" name="alarmSmsProviderAppId">
                    <Input placeholder="服务商 AppId"/>
                </Form.Item>

                <Form.Item label="AppSecret" name="alarmSmsProviderAppSecret">
                    <Input placeholder="服务商 App Secret"/>
                </Form.Item>

                <Form.Item>
                    <Button type="primary" loading={submitLoading} onClick={() => handleSubmit()}>提交</Button>
                </Form.Item>

            </Form>
        </>
    );
};

export default SysOption;
