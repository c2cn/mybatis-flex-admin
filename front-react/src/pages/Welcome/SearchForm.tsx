import React from 'react';
import {
    Button,
    Col,
    Form,
    Input,
    Row,
    theme,
} from 'antd';
import {useSearchParams} from "react-router-dom";
import {removeEmpty} from "../../lib/commons";

const SearchForm: React.FC<{ onSearch: (values: any) => void }>
    = (props) => {


    const [searchParams, setSearchParams] = useSearchParams();

    const {token} = theme.useToken();
    const [form] = Form.useForm();

    const formStyle = {
        maxWidth: 'none',
        background: token.colorFillAlter,
        borderRadius: token.borderRadiusLG,
        padding: 24,
        marginBottom: '20px'

    };

    const onFinish = (values: any) => {
        props.onSearch(removeEmpty(values));
    };

    return (
        <Form form={form} style={formStyle} onFinish={onFinish}>
            <Row gutter={24}>

                <Col span={6}>
                    <Form.Item name="platform" label="平台" initialValue={ searchParams.get('platform') || ""}>
                        <Input placeholder="请输入平台名称" />
                    </Form.Item>
                </Col>

                <Col span={6}>
                    <Form.Item name="hostIp" label="执行主机IP" initialValue={searchParams.get('hostIp') || ""}>
                        <Input placeholder="请输入 IP 地址" />
                    </Form.Item>
                </Col>

                <Col span={6}>
                    <Form.Item name="module" label="模块" initialValue={searchParams.get('module') || ""}>
                        <Input placeholder="请输入模块名称"/>
                    </Form.Item>
                </Col>

                <Col span={6}>
                    <Form.Item name="url" label="URL地址" initialValue={searchParams.get('url') || ""}>
                        <Input placeholder="请输入URL地址"/>
                    </Form.Item>
                </Col>

                <Col span={6}>
                    <Form.Item name="user" label="用户" initialValue={searchParams.get('user') || ""}>
                        <Input placeholder="请输入执行用户"/>
                    </Form.Item>
                </Col>

                <Col span={6}>
                    <Form.Item name="userIp" label="用户IP地址" initialValue={searchParams.get('userIp') || ""}>
                        <Input placeholder="请输入用户 IP 地址"/>
                    </Form.Item>
                </Col>

                <Col span={12} style={{textAlign: 'right'}}>
                    <Button type="primary" htmlType="submit">
                        搜索
                    </Button>
                    <Button
                        style={{margin: '0 8px'}}
                        onClick={() => {
                            form.resetFields();
                        }}
                    >
                        重置
                    </Button>
                </Col>
            </Row>
        </Form>
    );
};

export default SearchForm