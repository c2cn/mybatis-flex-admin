import React from 'react';
import {
    ColumnHeightOutlined, DownloadOutlined,
    DownOutlined, EyeOutlined, FormatPainterOutlined,
    ReloadOutlined,
    SwapOutlined,
} from '@ant-design/icons';
import {
    Space,
    Table,
} from 'antd';
import {ColumnsType} from "antd/es/table";
import AdvancedSearchForm from "./SearchForm";
import {useAuditMessagePage} from "../../service/AuditMessageService";
import {useSearchParams} from "react-router-dom";


const columns:ColumnsType<AuditMessage> = [
    {
        title: '平台',
        dataIndex: 'platform',
        key: 'platform',
    },
    {
        title: '模块',
        dataIndex: 'module',
        key: 'module',
    },
    {
        title: '用户',
        dataIndex: 'user',
        key: 'user',
    },
    {
        title: '用户IP',
        dataIndex: 'userIp',
        key: 'userIp',
    },
    {
        title: '执行SQL',
        dataIndex: 'query',
        key: 'query',
        width: "40%",
    },
    {
        title: '执行时间',
        dataIndex: 'queryTime',
        key: 'queryTime',
    },
    {
        title: '消耗时间',
        dataIndex: 'elapsedTime',
        key: 'elapsedTime',
    },
    {
        title: '操作',
        dataIndex: 'action',
        key: 'action',
        fixed: 'right',
        width: 150,
        render: () => {
            return (
                <Space>
                    <a><EyeOutlined/> 详情</a>
                    <a> 更多 <DownOutlined style={{fontSize: "10px"}}/></a>
                </Space>
            )
        }
    },
];

const Welcome: React.FC = () => {

    const [searchParams, setSearchParams] = useSearchParams();

    const pageParams = {pageSize: 10} as any;
    searchParams.forEach((value, key) => {
        pageParams[key] = value;
    })

    const {isLoading, data} = useAuditMessagePage(pageParams);


    const onSearch = (values: any) => {
        setSearchParams(values);
    }

    const onChange = (pagination: number) => {
        setSearchParams({
            ...pageParams,
            pageNumber: pagination,
        })
    }


    return (
        <div>
            <AdvancedSearchForm onSearch={onSearch}/>

            <Space style={{display: "flex", justifyContent: "space-between", padding: "10px 0"}}>

                <Space align={"center"} size={"middle"}>
                    <ReloadOutlined/>
                    <DownloadOutlined/>
                    <ColumnHeightOutlined/>
                    <FormatPainterOutlined/>
                    <SwapOutlined/>
                </Space>

            </Space>

            <Table loading={isLoading}
                   dataSource={data?.records || []}
                   columns={columns}
                   rowKey={(record) => record.id}
                   pagination={
                       {
                           showQuickJumper: true,
                           defaultCurrent: data?.pageNumber || 1,
                           total: data?.totalRow || 0,
                           showTotal: (total) => `共 ${total} 条数据`,
                           onChange
                       }
                   }
            />
        </div>
    )
}
export default Welcome