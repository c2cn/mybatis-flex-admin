import {
    KeyOutlined,
    SettingOutlined, TeamOutlined,
    UnorderedListOutlined, UserOutlined,
} from "@ant-design/icons";
import lazyLoad from "../lazyLoad";
import React, {lazy} from "react";
import {MenuRouteObject} from "../router";
import AccountList from "../../pages/System/AccountList";
import Profile from "../../pages/System/Profile";
import UpdatePassword from "../../pages/System/UpdatePassword";

const system: MenuRouteObject = {
    path: "system",
    label: "menu.system management",
    icon: <SettingOutlined/>,
    children: [
        {
            path: "accounts",
            label: "账户列表",
            icon: <TeamOutlined/>,
            element: <AccountList/>
        },
        {
            path: "profile",
            label: "个人资料",
            icon: <UserOutlined/>,
            element: <Profile/>
        },
        {
            path: "password",
            label: "修改密码",
            icon: <KeyOutlined/>,
            element: <UpdatePassword/>
        },
        {
            path: "options",
            label: "系统设置",
            icon: <SettingOutlined/>,
            element: lazyLoad(lazy(() => import("../../pages/System/SysOptions/index")))
        },
    ] as MenuRouteObject[]
}
export default system