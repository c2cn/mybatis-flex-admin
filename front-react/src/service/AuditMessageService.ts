import {useQuery} from "@tanstack/react-query";
import axios from "axios";


export const useAuditMessagePage = (params: any) => {
    return useQuery(["useAuditMessagePage",params], async () => {
        const resp = await axios.post('/auditMessage/page', params).catch((err) => {
            return err;
        });
        return resp.data ? resp.data : resp;
    })
}

