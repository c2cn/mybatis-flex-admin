
![](./docs/assets/images/index.png)

# MyBatis-Flex-Admin

**MyBatis-Flex-Admin** 是一个基于 MyBatis-Flex 开发的 SQL 执行日志平台和 SQL 审计平台，整个项目只有 5 张表，也是一个绝佳的 MyBatis-Flex 学习项目。

该项目使用前后端分离的方式，后端使用 SpringBoot 2.x + MyBatis-Flex，前台有 React 和 Vue 两个版本。


## 什么是 SQL 审计

SQL 审计是指对 SQL 语句的执行情况进行记录和追踪，其中包括 SQL 的执行时间、执行消耗和执行结果等信息。


通过 SQL 审计，我们可以对数据的使用情况进行监控和管理，包括对 SQL 注入、非法访问、数据泄露等安全问题的检测和防范，是企业数据安全体系的重要组成部分。
同时、审计平台提供 SQL 访问日志长期存储，满足等保合规要求。


## 为什么要做 SQL 审计平台 MyBatis-Flex-Admin

就目前的市场而言：SQL 审计功能，一般是通过监听数据库的执行日志来实现的，这种方式往往功能有限的，因为数据库端只能获取 SQL 的执行内容、执行时间以及结果。


但是，在等保安全体系的要求下，由数据库端提供的 SQL 审计信息（或功能）往往是不够的，比如产生这个 SQL 的应用、平台、以及相关的网页 URL 地址，
甚至包括产生这个 SQL 涉及的用户以及用户 IP 地址等其他相关信息，数据库端往往无法获取。


因此，MyBatis-Flex-Admin 应运而生。



## 后台涉及技术栈

- SpringBoot v2.x
- MyBatis-Flex

## React 版本涉及相关技术栈

- TypeScript 
- React 18 
- Ant Design v5.x
- Vite 4.x
- React Router v6.x
- Zustand
- Tanstack Query + Axios
- I18next

## Vue 版本涉及相关技术栈

- TypeScript
- Vue3
- Vue Router
- Pinia
- Arco Design v2.x
- Vite 4.x
- Axios