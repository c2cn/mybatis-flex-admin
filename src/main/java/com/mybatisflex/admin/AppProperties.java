package com.mybatisflex.admin;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppProperties {

    public static String jwtToken;

    public static String messageSecretKey;


    @Value("${mybatis-flex.admin.jwt-token}")
    public void setJwtToken(String jwtToken){
        AppProperties.jwtToken = jwtToken;
    }



    @Value("${mybatis-flex.admin.message-secret-key}")
    public void setMessageSecretKey(String messageSecretKey){
        AppProperties.messageSecretKey = messageSecretKey;
    }


}
