package com.mybatisflex.admin.controller;

import com.mybatisflex.admin.entity.AuditMessage;
import com.mybatisflex.admin.service.AuditMessageService;
import com.mybatisflex.admin.spring.jsonbody.JsonBody;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;

import static com.mybatisflex.admin.entity.table.AuditMessageTableDef.AUDIT_MESSAGE;

/**
 * 控制层。
 *
 * @author michael
 * @since 2023-07-01
 */
@RestController
@RequestMapping("/api/auditMessage")
public class AuditMessageController {

    @Autowired
    private AuditMessageService auditMessageService;


    /**
     * 根据主键获取详细信息。
     *
     * @param id 主键
     * @return 详情
     */
    @GetMapping("detail/{id}")
    public AuditMessage detail(@PathVariable Serializable id) {
        return auditMessageService.getById(id);
    }

    /**
     * 分页查询。
     *
     * @param page 分页对象
     * @return 分页对象
     */
    @PostMapping("page")
    public Page<AuditMessage> page(@JsonBody Page<AuditMessage> page
            , @JsonBody("platform") String platform
            , @JsonBody("hostIp") String hostIp
            , @JsonBody("module") String module
            , @JsonBody("url") String url
            , @JsonBody("user") String user
            , @JsonBody("userIp") String userIp
    ) {
        QueryWrapper qw = QueryWrapper.create()
                .where(AUDIT_MESSAGE.PLATFORM.eq(platform))
                .and(AUDIT_MESSAGE.HOST_IP.eq(hostIp))
                .and(AUDIT_MESSAGE.MODULE.eq(module))
                .and(AUDIT_MESSAGE.URL.eq(url))
                .and(AUDIT_MESSAGE.USER.eq(user))
                .and(AUDIT_MESSAGE.USER_IP.eq(userIp));
        return auditMessageService.page(page, qw);
    }

}