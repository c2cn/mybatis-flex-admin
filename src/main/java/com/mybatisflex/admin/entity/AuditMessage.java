package com.mybatisflex.admin.entity;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 *  实体类。
 *
 * @author michael
 * @since 2023-07-07
 */
@Table(value = "tb_audit_message")
public class AuditMessage implements Serializable {

    /**
     * 主键
     */
    @Id(keyType = KeyType.Generator, value = "snowFlakeId")
    private BigInteger id;

    /**
     * 平台，或者是应用
     */
    private String platform;

    /**
     * 模块
     */
    private String module;

    /**
     * url id
     */
    private String url;

    /**
     * 平台用户
     */
    private String user;

    /**
     * 平台用户 IP 地址
     */
    private String userIp;

    /**
     * 服务器 IP
     */
    private String hostIp;

    /**
     * 执行的 sql
     */
    private String query;

    /**
     * 执行的 sql 的参数
     */
    private String queryParams;

    /**
     * 执行时间
     */
    private Date queryTime;

    /**
     * 执行消耗时间
     */
    private Long elapsedTime;

    /**
     * 创建时间
     */
    private Date created;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    public String getHostIp() {
        return hostIp;
    }

    public void setHostIp(String hostIp) {
        this.hostIp = hostIp;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQueryParams() {
        return queryParams;
    }

    public void setQueryParams(String queryParams) {
        this.queryParams = queryParams;
    }

    public Date getQueryTime() {
        return queryTime;
    }

    public void setQueryTime(Date queryTime) {
        this.queryTime = queryTime;
    }

    public Long getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(Long elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

}
