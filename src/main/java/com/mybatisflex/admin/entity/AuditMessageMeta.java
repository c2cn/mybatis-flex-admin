package com.mybatisflex.admin.entity;

import com.mybatisflex.annotation.Table;
import java.io.Serializable;
import java.math.BigInteger;

/**
 *  实体类。
 *
 * @author michael
 * @since 2023-07-07
 */
@Table(value = "tb_audit_message_meta")
public class AuditMessageMeta implements Serializable {

    
    private BigInteger messageId;

    
    private String metaKey;

    
    private String metaValue;

    public BigInteger getMessageId() {
        return messageId;
    }

    public void setMessageId(BigInteger messageId) {
        this.messageId = messageId;
    }

    public String getMetaKey() {
        return metaKey;
    }

    public void setMetaKey(String metaKey) {
        this.metaKey = metaKey;
    }

    public String getMetaValue() {
        return metaValue;
    }

    public void setMetaValue(String metaValue) {
        this.metaValue = metaValue;
    }

}
