package com.mybatisflex.admin.service;

import com.mybatisflex.admin.util.Result;
import com.mybatisflex.core.service.IService;
import com.mybatisflex.admin.entity.Account;

/**
 *  服务层。
 *
 * @author michael
 * @since 2023-07-01
 */
public interface AccountService extends IService<Account> {

    Result login(String account, String password);
}