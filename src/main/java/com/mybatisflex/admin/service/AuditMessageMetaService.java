package com.mybatisflex.admin.service;

import com.mybatisflex.core.service.IService;
import com.mybatisflex.admin.entity.AuditMessageMeta;

/**
 *  服务层。
 *
 * @author michael
 * @since 2023-07-01
 */
public interface AuditMessageMetaService extends IService<AuditMessageMeta> {

}