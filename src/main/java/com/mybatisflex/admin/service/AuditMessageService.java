package com.mybatisflex.admin.service;

import com.mybatisflex.core.service.IService;
import com.mybatisflex.admin.entity.AuditMessage;

/**
 *  服务层。
 *
 * @author michael
 * @since 2023-07-01
 */
public interface AuditMessageService extends IService<AuditMessage> {

}