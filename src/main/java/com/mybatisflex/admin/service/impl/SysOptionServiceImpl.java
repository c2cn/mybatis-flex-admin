package com.mybatisflex.admin.service.impl;

import com.mybatisflex.admin.entity.SysOption;
import com.mybatisflex.admin.mapper.SysOptionMapper;
import com.mybatisflex.admin.service.SysOptionService;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Map;

import static com.mybatisflex.admin.entity.table.SysOptionTableDef.SYS_OPTION;

/**
 * 服务层实现。
 *
 * @author michael
 * @since 2023-07-01
 */
@Service
public class SysOptionServiceImpl extends ServiceImpl<SysOptionMapper, SysOption> implements SysOptionService {

    @Override
    public void batchUpdate(Map<String, Object> map) {
        if (map == null || map.isEmpty()) {
            return;
        }

        map.forEach((key, value) -> {
            SysOption option = getOne(SYS_OPTION.KEY.eq(key));
            if (option == null) {
                option = new SysOption();
                option.setKey(key);
            }
            option.setValue(value == null ? "" : value.toString());
            saveOrUpdate(option);
        });
    }
}