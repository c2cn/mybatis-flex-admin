package com.mybatisflex.admin.util;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ResponseUtil {

    public static void response(HttpServletResponse response,String text){
        response.setContentType("application/json; charset=utf-8");
        try {
            response.getWriter().write(text);
        } catch (IOException e) {
           //ignore
        }
    }
}
